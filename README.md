# README

A dockerized Rails API-Only App (TODO: Add React)

## Prerequisites

* Docker (preferably Docker Desktop, which bundles docker-compose)
* Gitlab account
  * Personal Access token with `api` and `read_registry` permissions (if you want to deploy a container and use Gitlab for CI)

## Architecture

* The docker-compose.yml expects the component repositories to be children of this directory
* Backend: Rails
  * **SUPER IMPORTANT** This is configured to use UUIDs for primary keys in the generators: rails/config/initializers/generators.rb
  * Rubocop
  * Data Migrations: https://github.com/ilyakatz/data-migrate
  * RSpec
    * SimpleCov
    * Shoulda Matchers
    * DatabaseCleaner
    * FactoryBot
    * Faker
  * v1 API Routes returning JSON
  * Postgres DB
* Frontend: TBD (probably React)

## Assumptions, Assertions, and Comments

* I decided to go with a monorepo because of previous experience managing multi-repo projects.  If you need to make changes to multiple layers of the application, creating and managing multiple branches on multiple repos is more disruptive than handling merge conflicts, in my experience.  With a monorepo, everything you need to code review a PR is in the same place, and it makes it easier to track changes that impacted multiple layers of the application.
* Right now this *does* cache data and *attempts* to cache gems - the method used is pretty naive and I'd like a better one.
* I tend to prefer to use non-standard ports on the host machine in case there are local instances of anything running on their default ports, so Rails and Postgres are on different ports (see the `docker-compose` file)

## Get Started

First run:

```
git@gitlab.com:katelovescode/dockerized-rails-react.git
cd dockerized-rails-react
docker-compose build
docker-compose run --rm api bundle install
docker-compose run --rm api rails db:setup
docker-compose up
```

Visit `localhost:3333` to see Rails running. 🥳

Safely take down the containers:
```
docker-compose down
```

Every time after:
```
docker-compose up
```

## Developing with Docker-Compose

Actions you would normally take in a development environment (e.g. `bundle exec rails console`, or `bundle exec rails db:migrate`, or `bundle exec rails g migration MyMigrationName` or `yarn install`) will need to be run in the context of the docker container.  The absolute easiest way to do this is by running `docker-compose exec` with the name of the service:

```
docker-compose exec api rails db:migrate
```

**NB: Because of the configuration of the Dockerfile (which adds binstubs for bundler in the container), you do not have to use `bundle exec`.**

This can get type-heavy.  You can install `oh-my-zsh` and use the `docker-compose` plugin to give you access to some shortcuts for docker-compose commands.  With that plugin, the above command becomes:

```
dce api rails db:migrate
```

It's also recommended to locally alias your most common commands.

### Debugging in Running Containers

You can use `binding.pry` but then you'll have to `exec` into the container and the behavior isn't always predictable.  My best experience is using `binding.pry_remote` and opening a new terminal to run `pry-remote` which won't disrupt the container service.

## Advanced Docker Use

Coming soon.

## Deployments and Dependencies

If you add a dependency to Rails, you'll need to manually rebuild the CI image and push it - we use it to run tests on Gitlab:
```
docker login registry.gitlab.com
> this will ask for your username and password (use your personal access token)

# From the project root - this builds a rails-only image
docker build -t registry.gitlab.com/[organization]/[repo]/[image] -f rails/ci.Dockerfile rails 
docker push registry.gitlab.com/[organization]/[repo]/[image]
```

`.gitlab-ci-yml` contains the configuration for deploying to Gitlab, running rubocop and rspec, and setting up the test db in the CI environment; note the copying of `database.gitlab.yml` to `database.yml` - your local tests need your local test configuration to run.  The CI tests need a different configuration; that's why this step happens.  It will also push to Heroku if you set your `HEROKU_APP_STAGING` and `HEROKU_API_KEY` in Gitlab's Project > Settings > CI/CD > Variables - the Heroku `Procfile` as configured runs schema AND data migrations.  All of these actions only act on the Rails repo and will need to be replicated for other services as they are added.

## Resources/Further Reading

* [Ruby on Whales: Dockerizing Ruby and Rails development](https://evilmartians.com/chronicles/ruby-on-whales-docker-for-ruby-rails-development)
* [How do I deploy my code to Heroku using Gitlab CI/CD?](https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4)
* [7 Ways to Speed Up Gitlab CI/CD Times: Pre-install Dependencies](https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9#4bda)
* [Configuring a GitLab CI pipeline for Rails, PostgreSQL, rspec, and rubocop](https://medium.com/@steven.jackson/configuring-a-gitlab-ci-pipeline-for-rails-postgresql-and-rspec-57c411400172)