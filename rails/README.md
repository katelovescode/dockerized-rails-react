# README

* [Quickstart for Rails](https://docs.docker.com/compose/rails/)
* [9 Steps for Dockerizing a Rails API-Only Application](https://medium.com/@nirmalyaghosh/9-steps-for-dockerizing-a-rails-api-only-application-d65a8836f3df)
* [Cache Rails gems using docker-compose](https://dev.to/k_penguin_sato/cache-rails-gems-using-docker-compose-3o3f)
* [Creating staging and other environments in Rails](http://nts.strzibny.name/creating-staging-environments-in-rails/)
* [PosgreSQL UUID as primary key in Rails 5.1](https://clearcove.ca/2017/08/postgres-uuid-as-primary-key-in-rails-5-1)
* [Build a RESTful JSON API With Rails 5 - Part One](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)
* [Build a RESTful JSON API With Rails 5 - Part Two](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-two)

# TODO

* Token auth: [Token-based Authentication with Ruby on Rails 5 API](https://www.pluralsight.com/guides/token-based-authentication-with-ruby-on-rails-5-api)
* Current DB is in NoSQL so.........there's no data model.  Gonna have to intuit and work through it.
* Friendly IDs so the UUIDs don't get in the way: https://github.com/norman/friendly_id

## Notes

* Made some decisions:
  * re: names - full_name with a greeting_name is more culturally inclusive - UX will probably have to make it make sense but not everyone has one first name and one last name
  * Families have multiple guardians and children - so if a kid ages out or a different parent starts bringing the kid, we can keep records