# frozen_string_literal: true

# Base controller methods for API controllers
class ApplicationController < ActionController::API
  include Response

  before_action :authorize_request
  attr_reader :current_user

  private

  def authorize_request
    @current_user = Auth::AuthorizeApiRequest.new(request.headers).call[:user]
  end
end
