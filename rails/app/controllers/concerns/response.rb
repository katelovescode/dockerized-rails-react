# frozen_string_literal: true

# parses the response into JSON with a default OK status
module Response
  def json_response(object, status = :ok)
    render json: object, status: status
  end
end
